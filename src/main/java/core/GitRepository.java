import java.lang.Object;
import java.io.*;

public class GitRepository
{
  private String repository;
  public GitRepository(String repo)
  {
    repository = repo;
  }
  public String getHeadRef() throws FileNotFoundException
  {
    try
    {
      FileReader fr = new FileReader(repository+"/HEAD");
      BufferedReader bf = new BufferedReader(fr);
      return bf.readLine().substring(5);
    }
     catch (Exception e) {

     }
     return null;
  }

  public String getRefHash() throws FileNotFoundException
  {
    try
    {
      FileReader fr = new FileReader(repository+"/HEAD");
      BufferedReader bf = new BufferedReader(fr);
      String s = bf.readLine().substring(5);
      FileReader fr2 = new FileReader(s);
      BufferedReader bf2 = new BufferedReader(fr);
      return bf2.readLine();
    }
     catch (Exception e) {

     }
     return null;
  }

}
